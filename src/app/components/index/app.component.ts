

import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})


export class AppComponent {
	title = 'Mini App';

	
	hotelsList = [
	{	
		id: 1,
		nom: "Hotel Vincci Florat",
		lieu: "Hammamet",
		email: "VincciFlorat@gmail.com",
		note:5.2,
		PrixNuit:70,
		phone: " 70723123",
		address: "Rue el Ferdaous,2021",
		description:"hhh"
	},
	{
		id: 2,
		nom: "Hotel Movenpick",
		lieu: "Tunis",
		email: "Movenpick@gmail.com",
		note:8.4,
		PrixNuit:100,
		phone: " 71723123",
		address: "Rue el Ferdaous,2021",
		description:"L'hôtel Mövenpick Gammarth Tunis est un lieu unique et moderne pour les clients qui apprécient la perfection. Situé dans un quartier résidentiel exclusif, cet hôtel surplombe les magnifiques baies de la Méditerranée et les collines de Sidi Bou Said. L'hôtel est facilement accessible en voiture depuis l'aéroport"
	}
	];

	constructor() {
		
		localStorage.setItem('hotels', JSON.stringify(this.hotelsList));
	}
}


