
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

// Services
import { StudentService } from '../../../services/student/student.service';
import { routerTransition } from '../../../services/config/config.service';

@Component({
	selector: 'app-student-list',
	templateUrl: './student-list.component.html',
	styleUrls: ['./student-list.component.css'],
	animations: [routerTransition()],
	host: { '[@routerTransition]': '' }
})

export class StudentListComponent implements OnInit {
	hotelList: any;
	hotelListData: any;
	constructor(private studentService: StudentService, private toastr: ToastrService) { }
	
	ngOnInit() {
		this.getHotelList();
	}

	
	getHotelList() {
		const hotelList = this.studentService.getAllHotels();
		this.success(hotelList);
	}


	success(data) {
		this.hotelListData = data.data;
		for (let i = 0; i < this.hotelListData.length; i++) {
			this.hotelListData[i].name = this.hotelListData[i].nom + ' ' + this.hotelListData[i].lieu;
		}
	}

	
	deleteHotel(index: number) {
		const r = confirm('Are you sure?');
		if (r === true) {
			const hotelDelete = this.studentService.deleteHotel(index);
			if (hotelDelete) {
				this.toastr.success('Success', 'Hotel Deleted');
			}
			this.getHotelList();
		}
	}
}
