
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';

// Services
import { ValidationService } from '../../../services/config/config.service';
import { StudentService } from '../../../services/student/student.service';
import { routerTransition } from '../../../services/config/config.service';

import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-student-add',
	templateUrl: './student-add.component.html',
	styleUrls: ['./student-add.component.css'],
	animations: [routerTransition()],
	host: { '[@routerTransition]': '' }
})

export class StudentAddComponent implements OnInit {
	hotelAddForm: FormGroup;
	index: any;

	constructor(private formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private studentService: StudentService, private toastr: ToastrService) {

	
		this.route.params.subscribe(params => {
			this.index = params['id'];
			
			if (this.index && this.index !== null && this.index !== undefined) {
				this.getHotelDetails(this.index);
			} else {
				this.createForm(null);
			}
		});
	}

	ngOnInit() {
	}


	doRegister() {
		if (this.index && this.index !== null && this.index !== undefined) {
			this.hotelAddForm.value.id = this.index;
		} else {
			this.index = null;
		}
		const hotelRegister = this.studentService.doRegisterHotel(this.hotelAddForm.value, this.index);
		if (hotelRegister) {
			if (hotelRegister.code === 200) {
				this.toastr.success(hotelRegister.message, 'Success');
				this.router.navigate(['/feed']);
			} else {
				this.toastr.error(hotelRegister.message, 'Failed');
			}
		}
	}


	getHotelDetails(index: number) {
		const hotelDetail = this.studentService.getHotelDetails(index);
		this.createForm(hotelDetail);
	}


	createForm(data) {
		if (data === null) {
			this.hotelAddForm = this.formBuilder.group({
				nom: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
				lieu: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
				email: ['', [Validators.required, ValidationService.emailValidator]],
				phone: ['', [Validators.required, ValidationService.checkLimit(50000000, 99999999)]],
				address: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]]
			});
		} else {
			this.hotelAddForm = this.formBuilder.group({
				nom: [data.hotelData.nom, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
				lieu: [data.hotelData.lieu, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
				email: [data.hotelData.email, [Validators.required, ValidationService.emailValidator]],
				phone: [data.hotelData.phone, [Validators.required, ValidationService.checkLimit(50000000, 99999999)]],
				address: [data.hotelData.address, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
			});
		}
	}

}