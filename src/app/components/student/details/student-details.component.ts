
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RouterModule, Routes, Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { StudentService } from '../../../services/student/student.service';
import { routerTransition } from '../../../services/config/config.service';

@Component({
	selector: 'app-student-details',
	templateUrl: './student-details.component.html',
	styleUrls: ['./student-details.component.css'],
	animations: [routerTransition()],
	host: { '[@routerTransition]': '' }
})

export class StudentDetailsComponent implements OnInit {
	index: any;
	hotelDetail: any;
	constructor(private router: Router, private route: ActivatedRoute, private studentService: StudentService, private toastr: ToastrService) {
		
		this.route.params.subscribe(params => {
			this.index = params['id'];
			if (this.index && this.index != null && this.index !== undefined) {
				this.getHotelDetails(this.index);
			}
		});
	}

	ngOnInit() {
	}

	
	getHotelDetails(index: number) {
		const getHotelDetail = this.studentService.getHotelDetails(index);
		if (getHotelDetail) {
			this.hotelDetail = getHotelDetail.hotelData;
			this.toastr.success(getHotelDetail.message, 'Success');
		}
	}

}