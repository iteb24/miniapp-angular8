

import { Injectable } from '@angular/core';

@Injectable()
export class StudentService {

  constructor() { }

  
  getAllHotels() {
    let hotelList: any;
    if (localStorage.getItem('hotels') && localStorage.getItem('hotels') !== '') {
      hotelList = {
        code: 200,
        message: 'Hotels List Fetched Successfully',
        data: JSON.parse(localStorage.getItem('hotels'))
      };
    } else {
      hotelList = {
        code: 200,
        message: 'Hotels List Fetched Successfully',
        data: JSON.parse(localStorage.getItem('hotels'))
      };
    }
    return hotelList;
  }

  doRegisterHotel(data, index) {
    const hotelList = JSON.parse(localStorage.getItem('hotels'));
    let returnData;
    console.log('index', index);
    if (index != null) {
      for (let i = 0; i < hotelList.length; i++) {
        if (index !== i && hotelList[i].email === data.email) {
          returnData = {
            code: 503,
            message: 'Email Address Already In Use',
            data: null
          };
          return returnData;
        }
      }

      hotelList[index] = data;
      localStorage.setItem('hotels', JSON.stringify(hotelList));
      returnData = {
        code: 200,
        message: 'Student Successfully Updated',
        data: JSON.parse(localStorage.getItem('hotels'))
      };
    } else {
      data.id = this.generateRandomID();
      for (let i = 0; i < hotelList.length; i++) {
        if (hotelList[i].email === data.email) {
          returnData = {
            code: 503,
            message: 'Email Address Already In Use',
            data: null
          };
          return returnData;
        }
      }
      hotelList.unshift(data);

      localStorage.setItem('hotels', JSON.stringify(hotelList));

      returnData = {
        code: 200,
        message: 'Hotel Successfully Added',
        data: JSON.parse(localStorage.getItem('hotels'))
      };
    }
    return returnData;
  }

  deleteHotel(index: number) {
    const hotelList = JSON.parse(localStorage.getItem('hotels'));

    hotelList.splice(index, 1);

    localStorage.setItem('hotels', JSON.stringify(hotelList));

    const returnData = {
      code: 200,
      message: 'Hotel Successfully Deleted',
      data: JSON.parse(localStorage.getItem('hotels'))
    };

    return returnData;
  }



  getHotelDetails(index: number) {
    const hotelList = JSON.parse(localStorage.getItem('hotels'));

    const returnData = {
      code: 200,
      message: 'Hotel Details Fetched',
      hotelData: hotelList[index]
    };

    return returnData;
  }


  generateRandomID() {
    const x = Math.floor((Math.random() * Math.random() * 9999));
    return x;
  }

}